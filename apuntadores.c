#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

/* Apuntador genérico void * (sustituto de char *) */
/* * -> operador de indirección o des-referencia */
/* & -> operador que da la dirección de un objeto */

void pincremento(int* i)
{
    (*i)++;
    printf("i: %d\n", *i);
}

void incremento(int i)
{
    i++;
    printf("i: %d\n", i);
}

int main(void)
{
    int a;  // variable
    int* pa; // apuntador entero
    pa = &a; // apuntador hacia dirección de variable
    a = 10;  // nuevo valor de variable
    incremento(*pa); //Se da el valor de des-referencia 10 para poder operar
    pincremento(pa); //Se entrega el apuntador directamente
    printf("a: %d\n", a); // Se imprime el valor de la variable a
    return 0;
}

