/* Ejemplo de variables externas: variables que son */
/* definida por fuera de las funciones */

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

// definida como variable global
int8_t extVar = 0;
// también es una variable global pero será usada como externa
// a partir del id (nombre)
int8_t noExtVar = 0;

bool suma(void)
{
    // Definición de variable externa a la función
    extern int8_t extVar;
    // variable automática, solo existe en éste ámbito
    int8_t temp = 10;
    extVar = extVar + temp;
    return true;
}

int main(void)
{
    // variable externa
    extern int8_t extVar;
    // variable global
    noExtVar = 20;
    printf("extVar: %d\n", (int)extVar);
    printf("noExtVar: %d\n", (int)noExtVar);
    suma();
    printf("extVar: %d\n", (int)extVar);
    return 0;
}
