# Ejemplo de uso
```bash
gcc -c *.c
gcc -o test.bin *.o
./test.bin
```

o

```bash
 gcc -c *c && gcc -o test.bin *.o && ./test.bin
```

## Referencias

[Proceso de compulación con gcc](https://iie.fing.edu.uy/~vagonbar/gcc-make/gcc.htm)

