/* Una variable registro indica al compilador que esa variable será */
/* usada intensamente; ésto puede dar como resultado programas más */
/* pequeños y más rápidos, sin embargo, los compiladores pueden ignorar */
/* ésta sugerencia. */

/* Observaciones */
/* * La declaración register solo puede aplicar a variables automáticas (locales). */
/* * En la práctica que una variable sea admitida como registro por el compilador */
/*     pedenderá de las características de la máquina donde se ejecutará el programa. */
/* * No es posible tomar la dirección de una variable tipo registro sin importar */
/*     si en realidad la variable está en un registro o no. */ 

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

int main(int argc, char *argv[])
{
    register int8_t a = 2;
    register int8_t i;
    /* int8_t* pa = &a;  // no se puede acceder a la dirección de un registro */
    for (i = 0; i < 10; i++) {
        printf("%d\n", i*a);
    }
    return 0;
}
