/* Al aplicar static a un id (nombre para funciones o variables o constantes) */
/* éste solo será visible para el archivo donde ha */
/* sido creado */
/* Se limita el alcance del eobjeto exclusivamente al archivo fuente donde */
/* se está aplicando. */

#include "calc.h"

// ésta variable solo será alcanzada en éste archivo
static int8_t respuesta;

// La siguiente función es solo alcanzada en éste archivo no por
// fuera de él
static void suma(int8_t a)
{
    extern int8_t respuesta;
    respuesta += a;
}

// Ésta función si se puede usar en otros archivos
int8_t multiplicar(int8_t multiplicando, int8_t multiplicador)
{
    // se usa la variable externa como "variable global"
    extern int8_t respuesta;
    uint8_t i;
    for (i = 0; i < multiplicador; i++)
    {
        suma(multiplicando);
    }
    return respuesta;
}
