#include <stdio.h>
#include <string.h>

int main(void)
{
    char *pHello1 = "helloword from pointer";
    printf("%s\n", pHello1);
    printf("sise: %d\n", (int)strlen(pHello1));
    char hello2[] = "Hello from char" ;
    printf("%s\n", hello2);
    printf("sise: %d\n", (int)sizeof(hello2));
    return 0;
}
